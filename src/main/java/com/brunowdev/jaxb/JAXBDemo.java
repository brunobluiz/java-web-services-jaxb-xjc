package com.brunowdev.jaxb;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.brunowdev.paciente.Paciente;

public class JAXBDemo {

	public static void main(String... args) {

		try {

			JAXBContext context = JAXBContext.newInstance(Paciente.class);
			Marshaller marshaller = context.createMarshaller();

			Paciente paciente = new Paciente();
			paciente.setId(112358);
			paciente.setNome("David Duchovny");

			// Marshal
			StringWriter stringWriter = new StringWriter();
			marshaller.marshal(paciente, stringWriter);
			System.out.println(stringWriter);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			String pacienteMarshal = stringWriter.toString();

			// UnMarshal
			unmarshaller.unmarshal(new StringReader(pacienteMarshal));
			Paciente pacienteUnmarshall = (Paciente) unmarshaller.unmarshal(new StringReader(pacienteMarshal));
			System.out.println("Id: " + pacienteUnmarshall.getId());

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

}
