//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.09.07 at 09:58:45 PM BRT 
//


package com.brunowdev.paciente;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PagamentoTipo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PagamentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="dinheiro" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="planoSaude" type="{http://www.brunowdev.com/Paciente}PlanoSaude"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagamentoTipo", propOrder = {
    "planoSaude",
    "dinheiro"
})
public class PagamentoTipo
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    protected PlanoSaude planoSaude;
    protected Integer dinheiro;

    /**
     * Gets the value of the planoSaude property.
     * 
     * @return
     *     possible object is
     *     {@link PlanoSaude }
     *     
     */
    public PlanoSaude getPlanoSaude() {
        return planoSaude;
    }

    /**
     * Sets the value of the planoSaude property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanoSaude }
     *     
     */
    public void setPlanoSaude(PlanoSaude value) {
        this.planoSaude = value;
    }

    /**
     * Gets the value of the dinheiro property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDinheiro() {
        return dinheiro;
    }

    /**
     * Sets the value of the dinheiro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDinheiro(Integer value) {
        this.dinheiro = value;
    }

}
